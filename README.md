
```
gitlab-runner exec docker \
  --docker-volumes "$(pwd)/artifacts:/tmp/artifacts" \
    go_coverage

gitlab-runner exec docker \
    go_lint

gitlab-runner exec docker \
    go_test

gitlab-runner exec docker \
    go_vet

gitlab-runner exec docker \
  --docker-volumes "$(pwd)/artifacts:/tmp/artifacts" \
    go_build
```
