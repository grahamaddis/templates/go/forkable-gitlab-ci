package utils

import (
	"testing"
)

func TestGreeting(t *testing.T) {
	reference := "Hello y'all"
	result := Greeting()

	if result != reference {
		t.Errorf("Greeting shoutd be %s instead of %s", reference, result)
	}
}
