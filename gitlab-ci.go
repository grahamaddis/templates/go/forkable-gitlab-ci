package main

import (
    "fmt"
    
    "gitlab.com/grahamaddis/templates/go/forkable-gitlab-ci/pkg/utils"
)

// Topgreet returns a greeting
func Topgreet() string {
    return utils.Greeting()
}

func main() {
    
    fmt.Println(utils.Greeting())
    // fmt.Println("Hello")
}
